import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image, AsyncStorage } from 'react-native';
import { compose } from 'recompose';
import { promisify, connectUser, connectLinks } from '@AppRedux';
import styles from './styles';

class WelcomeScreen extends React.Component {

    componentWillMount() {

        if (!this.props.linksState.links || this.props.linksState.links.length == 0) {
            promisify(this.props.fetchLinks, {})
                .then((response) => {
                    console.log('Fetched links successfully', response);
                })
                .catch((err) => {
                    console.log('Fetch links failed with error: ', err);
                });
        }

        AsyncStorage.getItem('@knnkt:access_token')
            .then((token) => {
                if (token) {
                    promisify(this.props.getProfile, {})
                        .then((response) => {
                            this.props.navigation.navigate('Main');
                        })
                        .catch((err) => {
                            AsyncStorage.removeItem('@knnkt:access_token');
                            AsyncStorage.removeItem('@knnkt:refresh_token');
                        });
                }
            })
            .catch(() => { });
    }

    _handleLogin = () => {
        this.props.navigation.navigate('Login');
    }

    _handleSignup = () => {
        this.props.navigation.navigate('Signup');

    }

    render() {
        return (
            <View style={styles.container}>
                <Image style={styles.logo} source={require('@AppAssets/images/logo.png')} resizeMode={'contain'} />
                <TouchableOpacity onPress={this._handleLogin}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Log in</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this._handleSignup}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Sign up</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }
}

WelcomeScreen.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }),
}

export default compose(
    connectUser(),
    connectLinks(),
)(WelcomeScreen);