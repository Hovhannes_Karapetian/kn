import { StyleSheet, Dimensions } from 'react-native';
import { BACKGROUND_GREEN, BUTTON_GREEN } from '@AppConstants/colors';
const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: BACKGROUND_GREEN,
        justifyContent: 'center',
        paddingBottom: 120,
    },
    logo: {
        marginBottom: width * 0.1,
        width: width * 0.7,
        height: width * 0.4,
    },
    button: {
        marginTop: 20,
        width: width * 0.5,
        padding: 12,
        alignItems: 'center',
        backgroundColor: BUTTON_GREEN,
        borderRadius: 8,
    },
    buttonText: {
        color: '#FFF',
        fontSize: 20,
    },
});

export default styles;