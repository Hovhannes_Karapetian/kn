import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { SafeAreaView } from 'react-navigation';
import PhoneInput, { PhoneNumber } from '@AppComponents/PhoneInput';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import styles from './styles';
import { promisify, connectUser } from '@AppRedux';

class LoginScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      phoneNumber: '',
      password: '',
      spinnerVisible: false,
    };
  }

  _handleLogin = () => {
    const { phoneNumber, password } = this.state;

    if (!phoneNumber) {
      Toast.show("Please input phone number");
      return;
    }

    if (!password) {
      Toast.show("Please input password");
      return;
    }

    const rawNumber = phoneNumber.replace(/[+-\s]/g, '');

    this.setState({ spinnerVisible: true });
    promisify(this.props.userLogin, { phoneNumber: rawNumber, password })
      .then((res) => {
        console.log('login response', res);
        this.setState({ spinnerVisible: false });
        this.props.navigation.navigate('Main');
      })
      .catch((err) => {
        console.log('login error', err);
        this.setState({ spinnerVisible: false });
        Toast.show('Login failed');
      });
  }

  _handleBack = () => {
    this.props.navigation.goBack();
  }

  _handleChangePhoneNumber = (number) => {
    const rawNumber = number.replace(/[+-\s]/g, '');
    if (PhoneNumber.getDialCode(number) == "+1") {
      let newNumber = rawNumber.slice(0, 1);
      if (rawNumber.length > 1) {
        newNumber += " " + rawNumber.slice(1, 4);
      }
      if (rawNumber.length > 4) {
        newNumber += "-" + rawNumber.slice(4, 7);
      }
      if (rawNumber.length > 7) {
        newNumber += "-" + rawNumber.slice(7);
      }
      this.setState({ phoneNumber: newNumber });
    } else {
      this.setState({ phoneNumber: rawNumber });
    }
  }

  render() {
    const { phoneNumber, password, spinnerVisible } = this.state;

    return (
      <KeyboardAwareScrollView contentContainerStyle={{ flex: 1 }} enableOnAndroid>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
          <View style={styles.inner}>
            <Image style={styles.logo} source={require('@AppAssets/images/logo.png')} resizeMode={'contain'} />
            <View style={styles.inputContainer}>
              <PhoneInput
                onChangePhoneNumber={this._handleChangePhoneNumber}
                value={phoneNumber}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Password'}
                underlineColorAndroid={'transparent'}
                secureTextEntry={true}
                style={styles.input}
                value={password}
                onChangeText={(text) => this.setState({ password: text })}
                ref={(input) => this.passwordInput = input}
                returnKeyType={'go'}
                onSubmitEditing={this._handleLogin}
              />
            </View>
            <TouchableOpacity onPress={this._handleLogin}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Log in</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.backButton} onPress={this._handleBack}>
              <View style={styles.backButtonContent}>
                <Text style={styles.backButtonText}>{'< Back'}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spinner
            visible={spinnerVisible}
            textContent={'Logging in...'}
          />
        </SafeAreaView>
      </KeyboardAwareScrollView>
    )
  }
}

LoginScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }),
  userLogin: PropTypes.func.isRequired,
}

export default connectUser()(LoginScreen);
