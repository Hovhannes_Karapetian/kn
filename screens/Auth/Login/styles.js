import { StyleSheet, Dimensions } from 'react-native';
import { BACKGROUND_GREEN, BUTTON_GREEN } from '@AppConstants/colors';
const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BACKGROUND_GREEN,
    },
    inner: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        marginBottom: 20,
        width: width * 0.7,
        height: width * 0.4,
    },
    button: {
        marginTop: 40,
        width: width * 0.7,
        padding: 12,
        alignItems: 'center',
        backgroundColor: BUTTON_GREEN,
        borderRadius: 8,
    },
    buttonText: {
        color: '#FFF',
        fontSize: 20,
    },
    inputContainer: {
        backgroundColor: '#FFF',
        width: width * 0.7,
        height: 48,
        justifyContent: 'center',
        paddingHorizontal: 8,
        borderRadius: 12,
        marginTop: 20,
    },
    input: {
        fontSize: 16,
    },
    backButton: {
        position: 'absolute',
        left: 20,
        top: 0,
    },
    backButtonContent: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    backButtonText: {
        color: BUTTON_GREEN,
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default styles;