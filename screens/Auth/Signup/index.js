import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, TextInput } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { SafeAreaView } from 'react-navigation';
import PhoneInput, { PhoneNumber } from '@AppComponents/PhoneInput';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import styles from './styles';
import { promisify, connectUser } from '@AppRedux';

class SignupScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      phoneNumber: '',
      password: '',
      confirmPassword: '',
      spinnerVisible: false,
    };
  }

  _handleSignup = () => {
    const { firstName, lastName, phoneNumber, password, confirmPassword } = this.state;

    if (!firstName) {
      Toast.show("Please input first name");
      return;
    }

    if (!lastName) {
      Toast.show("Please input last name");
      return;
    }

    if (!phoneNumber) {
      Toast.show("Please input phone number");
      return;
    }

    if (!password) {
      Toast.show("Please input password");
      return;
    }

    if (password != confirmPassword) {
      Toast.show("Password does not match");
      return;
    }

    const rawNumber = phoneNumber.replace(/[+-\s]/g, '');

    this.setState({ spinnerVisible: true });
    promisify(this.props.userSignup, { firstName, lastName, phoneNumber: rawNumber, password })
      .then((res) => {
        console.log('signup response', res);
        this.setState({ spinnerVisible: false });
        this.props.navigation.navigate('Main');
      })
      .catch((err) => {
        console.log('signup error', err);
        this.setState({ spinnerVisible: false });
        Toast.show("Signup failed");
      });
  }

  _handleBack = () => {
    this.props.navigation.goBack();
  }

  _handleChangePhoneNumber = (number) => {
    const rawNumber = number.replace(/[+-\s]/g, '');
    if (PhoneNumber.getDialCode(number) == "+1") {
      let newNumber = rawNumber.slice(0, 1);
      if (rawNumber.length > 1) {
        newNumber += " " + rawNumber.slice(1, 4);
      }
      if (rawNumber.length > 4) {
        newNumber += "-" + rawNumber.slice(4, 7);
      }
      if (rawNumber.length > 7) {
        newNumber += "-" + rawNumber.slice(7);
      }
      this.setState({ phoneNumber: newNumber });
    } else {
      this.setState({ phoneNumber: rawNumber });
    }
  }

  render() {
    const { firstName, lastName, phoneNumber, password, confirmPassword, spinnerVisible } = this.state;

    return (
      <KeyboardAwareScrollView contentContainerStyle={{ flex: 1, }} enableOnAndroid>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
          <View style={styles.inner}>
            <Image style={styles.logo} source={require('@AppAssets/images/logo.png')} resizeMode={'contain'} />
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'First name'}
                underlineColorAndroid={'transparent'}
                style={styles.input}
                onChangeText={(text) => this.setState({ firstName: text })}
                value={firstName}
                returnKeyType={'next'}
                blurOnSubmit={false}
                onSubmitEditing={() => this.lastNameInput.focus()}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Last name'}
                underlineColorAndroid={'transparent'}
                style={styles.input}
                onChangeText={(text) => this.setState({ lastName: text })}
                value={lastName}
                ref={(ref) => this.lastNameInput = ref}
                returnKeyType={'next'}
                blurOnSubmit={false}
                onSubmitEditing={() => this.lastNameInput.blur()}
              />
            </View>
            <View style={styles.inputContainer}>
              <PhoneInput
                onChangePhoneNumber={this._handleChangePhoneNumber}
                value={phoneNumber}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Password'}
                underlineColorAndroid={'transparent'}
                secureTextEntry={true}
                style={styles.input}
                onChangeText={(text) => this.setState({ password: text })}
                value={password}
                ref={(ref) => this.passwordInput = ref}
                returnKeyType={'next'}
                blurOnSubmit={false}
                onSubmitEditing={() => this.confirmPasswordInput.focus()}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Confirm password'}
                underlineColorAndroid={'transparent'}
                secureTextEntry={true}
                style={styles.input}
                onChangeText={(text) => this.setState({ confirmPassword: text })}
                value={confirmPassword}
                ref={(ref) => this.confirmPasswordInput = ref}
                returnKeyType={'go'}
                onSubmitEditing={this._handleSignup}
              />
            </View>
            <TouchableOpacity onPress={this._handleSignup}>
              <View style={styles.button}>
                <Text style={styles.buttonText}>Sign up</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.backButton} onPress={this._handleBack}>
              <View style={styles.backButtonContent}>
                <Text style={styles.backButtonText}>{'< Back'}</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Spinner
            visible={spinnerVisible}
            textContent={'Registering...'}
          />
        </SafeAreaView>
      </KeyboardAwareScrollView>
    )
  }
}

SignupScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }),
  userSignup: PropTypes.func.isRequired,

}

export default connectUser()(SignupScreen);
