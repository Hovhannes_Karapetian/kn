import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, TextInput } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import styles from './styles';
import { promisify, connectUser } from '@AppRedux';

class ConfirmSMSScreen extends React.Component {

    _handleSubmit = () => {
        promisify(this.props.confirmSMS, {})
            .then((res) => {
                this.props.navigation.navigate('Main');
            })
            .catch((err) => {

            });
    }

    _handleBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
                <Image style={styles.logo} source={require('@AppAssets/images/logo.png')} resizeMode={'contain'} />
                <View style={styles.inputContainer}>
                    <TextInput
                        placeholder={'SMS confirmation code'}
                        underlineColorAndroid={'transparent'}
                        autoCapitalize={'none'}
                        style={styles.input}
                        keyboardType={'number-pad'}
                    />
                </View>
                <TouchableOpacity onPress={this._handleSubmit}>
                    <View style={styles.button}>
                        <Text style={styles.buttonText}>Submit</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.backButton} onPress={this._handleBack}>
                    <View style={styles.backButtonContent}>
                        <Text style={styles.backButtonText}>{'< Back'}</Text>
                    </View>
                </TouchableOpacity>
            </SafeAreaView>
        )
    }
}

ConfirmSMSScreen.propTypes = {
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
        goBack: PropTypes.func.isRequired,
    }),
    confirmSMS: PropTypes.func.isRequired,
}

export default connectUser()(ConfirmSMSScreen);
