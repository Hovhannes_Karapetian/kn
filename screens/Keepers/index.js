import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text } from 'react-native';
import styles from './styles';
import { compose } from 'recompose';
import { connectUser, connectKeepers, promisify } from '@AppRedux';
import { KeeperCard } from '@AppComponents';
import { filter } from 'lodash';

class KeepersScreen extends React.Component {

    componentWillMount() {
        this.getKeepers();
    }

    getKeepers = () => {
        promisify(this.props.getKeepers, {});
    }

    _keyExtractor = (item, index) => `${index}`;

    _renderItem = ({ item }) => {
        return (
            <KeeperCard data={item} onPress={() => this._handlePressKeeperItem(item)} onToggleKeeper={this._handleToggleKeeper} />
        )
    }

    _handleToggleKeeper = (keeper) => {
        const { keepers, id } = this.props.userState.currentUser;

        newKeepers = filter(keepers, (item) => {
            return item != keeper.id;
        });

        promisify(this.props.updateProfile, { keepers: newKeepers, userId: id })
            .then((res) => {
                this.getKeepers();
            });
    }

    _handlePressKeeperItem = (keeper) => {
        this.props.navigation.navigate('KeeperProfile', { keeper });
    }

    render() {
        const { allKeepers } = this.props.keepersState;
        return (
            <View style={styles.container}>
                <Text style={styles.title}>{'Keepers'}</Text>
                <FlatList
                    data={allKeepers}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}

KeepersScreen.propTypes = {
    keepersState: PropTypes.shape({
        allKeepers: PropTypes.arrayOf(PropTypes.shape({})),
    }),
    userState: PropTypes.shape({
        currentUser: PropTypes.shape({}),
    }),
};

export default compose(
    connectKeepers(),
    connectUser(),
)(KeepersScreen);