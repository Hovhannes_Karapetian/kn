import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text } from 'react-native';
import styles from './styles';
import { connectChats } from '@AppRedux';
import { ChatRow } from '@AppComponents';

class ChatListScreen extends React.Component {

    _keyExtractor = (item, index) => `${index}`;

    _renderItem = ({ item }) => {
        const { name } = item;
        return (
            <ChatRow data={item} onPress={() => this.props.navigation.navigate('Messages')} />
        )
    }

    render() {
        const { allChats } = this.props.chatsState;
        return (
            <View style={styles.container}>
                <FlatList
                    data={allChats}
                    renderItem={this._renderItem}
                    keyExtractor={this._keyExtractor}
                />
            </View>
        )
    }
}

ChatListScreen.propTypes = {
    chatsState: PropTypes.shape({
        allChats: PropTypes.arrayOf(PropTypes.shape({}))
    }),
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    })
}

export default connectChats()(ChatListScreen);