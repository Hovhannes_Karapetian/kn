import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import { LinearGradient } from 'expo';
import Swiper from 'react-native-swiper';
import { Ionicons } from '@expo/vector-icons';
import styles from './styles';
import { connectUser } from '@AppRedux';
import moment from 'moment';

class KeeperProfileScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    _renderLink = (link, index) => {
        return (
            <View
                key={`${index}`}
                style={styles.linkItem}
            >
                <LinearGradient
                    colors={['#62B794', '#4AA067']}
                    style={styles.linkItemInner}
                >
                    <Text style={styles.linkText}>{link.toUpperCase()}</Text>
                </LinearGradient>
            </View>
        );
    }

    _handlePressBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        const { keeper } = this.props.navigation.state.params;
        const { firstName, lastName, city, distance, bio, links, dob, pictures } = keeper;

        return (
            <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
                <ScrollView
                    style={styles.container}
                    contentContainerStyle={styles.contentContainer}
                >
                    <TouchableOpacity
                        onPress={this._handlePressBack}
                        style={styles.backButton}
                    >
                        <Ionicons name="ios-arrow-back" size={24} color={'#888'} />
                    </TouchableOpacity>
                    <Swiper
                        height={300}
                        showsPagination={true}
                        paginationStyle={{ position: 'absolute', top: 10, bottom: null, }}
                        dotStyle={{ backgroundColor: 'transparent', borderWidth: 1, borderColor: 'white', }}
                        activeDotColor={'white'}
                    >
                        {
                            pictures.map((picture, index) => <Image
                                key={index}
                                style={styles.profileImage}
                                resizeMode={'cover'}
                                source={{ uri: picture }}
                            />)
                        }
                    </Swiper>
                    <Text style={styles.nameText}>
                        {`${firstName} ${lastName}`}
                        {
                            dob && <Text> ({moment().diff(moment(dob), 'years')})</Text>
                        }
                    </Text>
                    {
                        city &&
                        <Text style={styles.addressText}>
                            {city}
                            {distance && ` (${distance}mi)`}
                        </Text>
                    }
                    {
                        bio &&
                        <Text style={styles.bioText}>{bio}</Text>
                    }
                    <View style={styles.linksContainer}>
                        {
                            links.map((link, index) => this._renderLink(link, index))
                        }
                    </View>
                </ScrollView>
            </SafeAreaView>
        )
    }
}

KeeperProfileScreen.propTypes = {
    userState: PropTypes.shape({}),
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    })
}

export default connectUser()(KeeperProfileScreen);