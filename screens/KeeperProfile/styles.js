import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        padding: 20,
        paddingTop: 0,
    },

    profileImage: {
        flex: 1,
        borderRadius: 4,
    },
    nameText: {
        marginTop: 12,
        fontSize: 20,
    },
    addressText: {
        marginTop: 12,
        fontSize: 18,
    },
    bioText: {
        marginTop: 12,
        fontSize: 14,
        letterSpacing: 0.5,
    },
    linksContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 8,
        marginHorizontal: -6,
    },
    linkItem: {
        marginTop: 12,
        marginHorizontal: 6,
        flexGrow: 1,
        alignItems: 'center',
    },
    linkItemInner: {
        width: '100%',
        padding: 8,
        borderRadius: 4,
    },
    linkText: {
        color: '#FFF',
        fontSize: 14,
        textAlign: 'center',
    },
    backButton: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#FFF',
        shadowOffset: { width: 0, height: 2, },
        shadowColor: '#888',
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 20,
    }
});

export default styles;