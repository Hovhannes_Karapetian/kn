import { StyleSheet, Dimensions } from 'react-native';
import { BACKGROUND_GREEN } from '@AppConstants/colors';
const { width, height } = Dimensions.get('window');

const green = 'rgba(67,174,47,1)';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BACKGROUND_GREEN,
    },
    contentContainer: {
        flex: 1,
        padding: 20,
    },
    header: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    title: {
        marginTop: 8,
        fontSize: 24,
        textAlign: 'center',
    },
    doneButton: {
        position: 'absolute',
        right: 0,
    },
    doneButtonText: {
        fontSize: 20,
        color: green,
    },
    inputContainer: {
        backgroundColor: '#FFF',
        justifyContent: 'center',
        padding: 12,
        borderRadius: 12,
        marginTop: 20,
    },
    input: {
        fontSize: 16,
    },
    bioInput: {
        fontSize: 16,
        height: 120,
    },
    imageListContainer: {
        height: width * 0.6,
    },
    imageCellContainer: {
        flex: 1,
        aspectRatio: 1,
        padding: 8,
    },
    imageCellImage: {
        width: '100%',
        height: '100%',
        borderRadius: 8,
    },
    imageCellDeleteButton: {
        position: 'absolute',
        left: 0,
        top: 0,
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: 'rgba(255,98,79,1)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    linkDeleteButton: {
        position: 'absolute',
        left: -8,
        top: -8,
        width: 24,
        height: 24,
        borderRadius: 12,
        backgroundColor: 'rgba(255,98,79,1)',
        alignItems: 'center',
        justifyContent: 'center',
    },
    emptyView: {
        width: '100%',
        height: '100%',
        borderRadius: 12,
        borderWidth: 4,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: green,
    },
    emptyAddIcon: {
        color: green,
        fontSize: 24,
    },
    linksContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 8,
        marginHorizontal: -6,
    },
    linkItem: {
        marginTop: 12,
        marginHorizontal: 6,
        flexGrow: 1,
        alignItems: 'center',
    },
    linkItemInner: {
        width: '100%',
        padding: 8,
        borderRadius: 4,
    },
    linkText: {
        color: '#FFF',
        fontSize: 14,
        textAlign: 'center',
    },
})

export default styles;