import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text, TouchableOpacity, TextInput, Image, Platform } from 'react-native';
import { Permissions, ImagePicker, LinearGradient } from 'expo';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DateTimePicker from 'react-native-modal-datetime-picker';
import { SafeAreaView } from 'react-navigation';
import { Ionicons, } from '@expo/vector-icons';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './styles';
import { promisify, connectUser, } from '@AppRedux';
import { compose } from 'recompose';
import Toast from 'react-native-root-toast';
import moment from 'moment';

const ImageCell = ({ source, onDelete }) => {
  return (
    <View style={styles.imageCellContainer}>
      <Image source={{ uri: source }} style={styles.imageCellImage} />
      <TouchableOpacity style={styles.imageCellDeleteButton} onPress={onDelete}>
        <Ionicons name={'ios-close'} size={18} />
      </TouchableOpacity>
    </View>
  )
};

const EmptyCell = ({ onPress }) => {
  return (
    <TouchableOpacity style={styles.imageCellContainer} onPress={onPress}>
      <View style={styles.emptyView}>
        <Ionicons name={'ios-add'} style={styles.emptyAddIcon} />
      </View>
    </TouchableOpacity>
  )
};

const LinkItem = ({ link, onDelete }) => {
  return (
    <View
      style={styles.linkItem}
      onPress={() => onDelete(link)}
    >
      <LinearGradient
        colors={['#62B794', '#4AA067']}
        style={styles.linkItemInner}
      >
        <Text style={styles.linkText}>{link.toUpperCase()}</Text>
      </LinearGradient>
      <TouchableOpacity style={styles.linkDeleteButton} onPress={onDelete}>
        <Ionicons name={'ios-close'} size={18} />
      </TouchableOpacity>
    </View>
  )
}

class EditProfileScreen extends React.Component {

  constructor(props) {
    super(props);

    const { firstName, lastName, dob, bio, pictures, links } = props.userState.currentUser;

    this.state = {
      firstName,
      lastName,
      dob,
      bio,
      pictures,
      links,
      spinnerVisible: false,
      spinnerText: '',
      datePickerVisible: false,
    };
  }

  componentWillMount() {
  }

  _keyExtractor = (item, index) => `${index}`;

  _renderItem = ({ item, index }) => {
    const { pictures } = this.state;

    return pictures[index] ?
      <ImageCell source={pictures[index]} onDelete={() => this._handleDeleteImage(index)} />
      :
      <EmptyCell onPress={this._handlePickImage} />
  }

  _handleDeleteImage = (index) => {
    const { pictures } = this.state;
    const newPictures = pictures.slice();
    newPictures.splice(index, 1);
    this.setState({ pictures: newPictures });
  }

  _handlePickImage = async () => {

    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== 'granted') {
      alert('Permission not granted');
      return;
    }

    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      quality: 0.2,
    });

    if (!result.cancelled) {
      this._uploadImage(result);
    }
  }

  _uploadImage = (picture) => {
    const { currentUser } = this.props.userState;
    const uriParts = picture.uri.split('.');
    const fileType = uriParts[uriParts.length - 1];
    const data = new FormData();
    data.append("image", {
      name: `profile.${fileType}`,
      type: `image/${fileType}`,
      uri: picture.uri
    });
    this.setState({ spinnerVisible: true, spinnerText: 'Uploading image...' });
    promisify(this.props.uploadProfilePicture, { data, userId: currentUser.id })
      .then((res) => {
        console.log('profile picture uploaded successfully', res);
        const { pictures } = this.state;
        this.setState({ spinnerVisible: false, pictures: pictures.concat([res.url]) });
      })
      .catch((err) => {
        console.log('profile picture upload failed', err);
        this.setState({ spinnerVisible: false });
      });
  }

  _handleDeleteLink = (index) => {
    const { links } = this.state;
    const newLinks = links.slice();
    newLinks.splice(index, 1);
    this.setState({ links: newLinks });
  }

  _handlePressDone = () => {
    const { firstName, lastName, dob, bio, pictures, links } = this.state;
    const { currentUser } = this.props.userState;
    const { id } = currentUser;
    let updateParams = {};
    let needsUpdate = false;

    if (firstName !== currentUser.firstName) {
      updateParams = { ...updateParams, firstName };
      needsUpdate = true;
    }

    if (lastName !== currentUser.lastName) {
      updateParams = { ...updateParams, lastName };
      needsUpdate = true;
    }

    if (dob !== currentUser.dob) {
      updateParams = { ...updateParams, dob };
      needsUpdate = true;
    }

    if (bio !== currentUser.bio) {
      updateParams = { ...updateParams, bio };
      needsUpdate = true;
    }

    if (pictures !== currentUser.pictures) {
      updateParams = { ...updateParams, pictures };
      needsUpdate = true;
    }

    if (links !== currentUser.links) {
      updateParams = { ...updateParams, links };
      needsUpdate = true;
    }

    if (needsUpdate) {
      this.setState({ spinnerVisible: true, spinnerText: 'Updating profile...' });
      promisify(this.props.updateProfile, {
        ...updateParams,
        userId: id,
      })
        .then((res) => {
          this.setState({ spinnerVisible: false });
          Toast.show('Profile updated successfully');
          this.props.navigation.goBack();
        })
        .catch((err) => {
          this.setState({ spinnerVisible: false });
          Toast.show('Failed to update profile');
        });
    } else {
      this.props.navigation.goBack();
    }
  }

  _handleDatePicked = (date) => {
    console.log(date);
    this.setState({ datePickerVisible: false, dob: date.toISOString() });
  }

  _hideDateTimePicker = () => {
    this.setState({ datePickerVisible: false });
  }

  _handlePressDob = () => {
    this.setState({ datePickerVisible: true });
  }

  render() {
    const { firstName, lastName, dob, bio, links, spinnerVisible, spinnerText, datePickerVisible } = this.state;
    return (
      <KeyboardAwareScrollView style={{ flex: 1 }} enableOnAndroid>
        <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
          <View style={styles.contentContainer}>
            <View style={styles.header}>
              <Text style={styles.title}>{'Edit Profile'}</Text>
              <TouchableOpacity
                style={styles.doneButton}
                onPress={this._handlePressDone}
              >
                <Text style={styles.doneButtonText}>{'Done'}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.imageListContainer}>
              <FlatList
                horizontal={false}
                numColumns={3}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderItem}
                data={[0, 1, 2, 3, 4, 5,]}
                style={styles.imageList}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'First Name'}
                underlineColorAndroid={'transparent'}
                style={styles.input}
                value={firstName}
                onChangeText={(text) => this.setState({ firstName: text })}
                returnKeyType={'next'}
                blurOnSubmit={false}
                onSubmitEditing={() => this.lastNameInput.focus()}
              />
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Last Name'}
                underlineColorAndroid={'transparent'}
                style={styles.input}
                value={lastName}
                onChangeText={(text) => this.setState({ lastName: text })}
                returnKeyType={'next'}
                blurOnSubmit={false}
                ref={(ref) => this.lastNameInput = ref}
                onSubmitEditing={() => this.lastNameInput.blur()}
              />
            </View>
            <View style={styles.inputContainer}>
              <TouchableOpacity onPress={this._handlePressDob}>
                <Text style={styles.input}>{dob ? moment(dob).format('MM-DD-YYYY') : 'Date of Birth'}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.inputContainer}>
              <TextInput
                placeholder={'Bio'}
                underlineColorAndroid={'transparent'}
                style={styles.bioInput}
                value={bio}
                onChangeText={(text) => this.setState({ bio: text })}
                multiline={true}
              />
            </View>
            <View style={styles.linksContainer}>
              {
                links.map((link, index) => <LinkItem
                  link={link}
                  onDelete={() => this._handleDeleteLink(index)}
                  key={index}
                />)
              }
            </View>
            <Spinner
              visible={spinnerVisible}
              textContent={spinnerText}
            />
            <DateTimePicker
              isVisible={datePickerVisible}
              onConfirm={this._handleDatePicked}
              onCancel={this._hideDateTimePicker}
            />
          </View>
        </SafeAreaView>
      </KeyboardAwareScrollView>
    );
  }
}

EditProfileScreen.propTypes = {
  userState: PropTypes.shape({
    currentUser: PropTypes.shape({})
  }),
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  })
}

export default compose(
  connectUser(),
)(EditProfileScreen);