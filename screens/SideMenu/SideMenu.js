import React from 'react';
import PropTypes from 'prop-types';
import { View, ScrollView, SafeAreaView, Text, Image, TouchableOpacity, Alert, Linking, Clipboard } from 'react-native';
import { compose } from 'recompose';
import Dialog from "react-native-dialog";
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import styles from './styles';
import { connectUser, connectLinks, connectSharedAction, promisify } from '@AppRedux';

class SideMenu extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            suggestDialogVisible: false,
            suggestLinkContent: '',
            spinnerVisible: false,
            feedbackDialogVisible: false,
        }
    }

    _handleEditProfile = () => {
        this.props.navigation.navigate('EditProfile');
    }

    _handleSignOut = () => {
        this.props.userLogout();
        this.props.navigation.navigate('Auth');
    }

    _handleFeedbackLink = () => {
        this.setState({ feedbackDialogVisible: true })
    }

    _handleSuggestLink = () => {
        this.setState({ suggestDialogVisible: true });
    }

    _handleSubmitSuggestLink = () => {
        this.setState({ suggestDialogVisible: false, spinnerVisible: true, });

        const { suggestLinkContent } = this.state;
        promisify(this.props.suggestLink, { link: suggestLinkContent })
            .then((res) => {
                this.setState({ spinnerVisible: false });
                setTimeout(() => {
                    Alert.alert('Thank you for submitting a new Link suggestion.', 'Your contributions are what makes Knnkt special. Your suggestion will be added once we\'ve received additional interest.');
                }, 400);
            })
            .catch((error) => {
                this.setState({ spinnerVisible: false });
                Toast.show('Failed to suggst link');
            });
    }
    
    _handleCancelSuggestLink = () => {
        this.setState({ suggestDialogVisible: false, });
    }

    _handleCancelFeedbackLink = () => {
        this.setState({ feedbackDialogVisible: false, });
    }

    _goToMailApp = () => {
        Linking.openURL('mailto:justinf@knnktapp.com?subject=Knnkt app feedback');
    }

    _mailClipboard = () => {
        Clipboard.setString('justinf@knnktapp.com')
        Toast.show('Copy email address')
    }

    render() {
        const { currentUser } = this.props.userState;
        const { pictures } = currentUser;
        const { suggestDialogVisible, spinnerVisible, feedbackDialogVisible } = this.state;

        return (
            <ScrollView c
                contentContainerStyle={styles.contentContainer}
                keyboardShouldPersistTaps={'always'}
            >
                <SafeAreaView>
                    <Image style={styles.profileImage} source={pictures && pictures.length > 0 ? { uri: pictures[0] } : require('@AppAssets/images/user.png')} />
                </SafeAreaView>
                <TouchableOpacity onPress={this._handleEditProfile}>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Edit Profile</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.editProfileBorder} />
                <TouchableOpacity onPress={this._handleSuggestLink}>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Suggest a Link</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Settings</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Premium Features</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this._handleFeedbackLink}>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Feedback</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Privacy Policy</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={this._handleSignOut}>
                    <View style={styles.drawerItem}>
                        <Text style={styles.drawerItemText}>Sign out</Text>
                    </View>
                </TouchableOpacity>
                <Dialog.Container visible={suggestDialogVisible}
                    useNativeDriver={true}
                    onBackdropPress={this._handleCancelSuggestLink}
                    avoidKeyboard={true}
                >
                    <Dialog.Title>{`Suggest a Link`}</Dialog.Title>
                    <Dialog.Input
                        autoFocus={true}
                        onChangeText={(text) => this.setState({ suggestLinkContent: text })}
                        onSubmitEditing={this._handleSubmitSuggestLink}
                        returnKeyType={'go'}
                    />
                    <Dialog.Button label="Submit" onPress={this._handleSubmitSuggestLink} />
                </Dialog.Container>

                <Dialog.Container visible={feedbackDialogVisible}
                    useNativeDriver={true}
                    onBackdropPress={this._handleCancelFeedbackLink}
                    avoidKeyboard={true}
                >
                    <Dialog.Title >{`Your feedback hepls make Knnkt better`}</Dialog.Title>
                    <Dialog.Description>{'Found a bug or have a feature request? Let us know about it!'}</Dialog.Description>
                    <View>
                        <Dialog.Button label="Copy email address" onPress={this._mailClipboard} />
                        <Dialog.Button label="Open mail app" onPress={this._goToMailApp} />
                    </View>
                </Dialog.Container>

                <Spinner
                    visible={spinnerVisible}
                />
            </ScrollView>
        );
    }
}

SideMenu.propTypes = {
    userState: PropTypes.shape({}),
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    }),
    userLogout: PropTypes.func.isRequired,
}

export default compose(
    connectUser(),
    connectLinks(),
    connectSharedAction()
)(SideMenu);
