import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    contentContainer: {
        padding: 20,
        alignItems: 'center',
    },
    profileImage: {
        marginTop: 40,
        width: 128,
        height: 128,
        borderRadius: 64,
    },
    drawerItem: {
        width: '100%',
        marginTop: 12,
        height: 48,
        justifyContent: 'center',
    },
    drawerItemText: {
        fontSize: 20,
    },
    editProfileBorder: {
        width: '100%',
        height: 1,
        backgroundColor: '#3CB080'
    }
});

export default styles;
