import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, } from 'react-native';
import styles from './styles';

import { SafeAreaView } from 'react-navigation';
import { GiftedChat } from 'react-native-gifted-chat';
import { ChatBubble } from '@AppComponents';

class MessagesScreen extends React.Component {

    state = {
        messages: [],
    }

    componentWillMount() {
        this.setState({
            messages: [
                {
                    _id: 1,
                    text: 'Hey, how\'s it going?',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Sarah Farley',
                    },
                },
                {
                    _id: 2,
                    text: 'Great! Wanna grab drinks?',
                    createdAt: new Date(),
                    user: {
                        _id: 1,
                    },
                },
                {
                    _id: 3,
                    text: 'I\'d love to! How about next Thursday?',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Sarah Farley',
                    },
                },
                {
                    _id: 4,
                    text: 'Works for me. Where at?',
                    createdAt: new Date(),
                    user: {
                        _id: 1,
                    },
                },
                {
                    _id: 5,
                    text: 'The Parlor?',
                    createdAt: new Date(),
                    user: {
                        _id: 2,
                        name: 'Sarah Farley',
                    },
                },
                {
                    _id: 6,
                    text: 'Sure thing! See you there 😉',
                    createdAt: new Date(),
                    user: {
                        _id: 1,
                    },
                },
            ].reverse(),
        })
    }

    onSend(messages = []) {
        this.setState(previousState => ({
            messages: GiftedChat.append(previousState.messages, messages),
        }))
    }

    _handleBack = () => {
        this.props.navigation.goBack();
    }

    _renderBubble = (props) => {
        return (
            <ChatBubble {...props} />
        );
    }
    render() {
        return (
            <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
                <View style={styles.userHeader}>
                    <Image
                        source={{ uri: 'https://images.unsplash.com/photo-1524593689594-aae2f26b75ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=80' }}
                        style={styles.userAvatar}
                        resizeMode={'cover'}
                    />
                    <Text style={styles.userName}>
                        Sarah Farley
                    </Text>
                    <TouchableOpacity style={styles.backButton} onPress={this._handleBack}>
                        <View style={styles.backButtonContent}>
                            <Text style={styles.backButtonText}>{'< Back'}</Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <GiftedChat
                    messages={this.state.messages}
                    onSend={messages => this.onSend(messages)}
                    renderAvatar={null}
                    user={{
                        _id: 1,
                    }}
                    renderBubble={this._renderBubble}
                    alignTop={true}
                />
            </SafeAreaView>
        )
    }
}

MessagesScreen.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func.isRequired,
    }),
}

export default MessagesScreen;
