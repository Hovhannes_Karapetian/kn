import { StyleSheet } from 'react-native';
import { BACKGROUND_GREEN, BUTTON_GREEN } from '@AppConstants/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 8,
    },
    userHeader: {
        height: 64,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    userAvatar: {
        width: 48,
        height: 48,
        borderRadius: 24,
    },
    userName: {
        marginLeft: 12,
        fontSize: 24,
    },
    backButton: {
        position: 'absolute',
        left: 8,
    },
    backButtonContent: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    backButtonText: {
        color: BUTTON_GREEN,
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default styles;