import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, Text, TouchableOpacity, ScrollView } from 'react-native';
import { LinearGradient } from 'expo';
import Swiper from 'react-native-swiper';
import styles from './styles';
import { connectUser } from '@AppRedux';
import moment from 'moment';

class MyProfileScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    _renderLink = (link, index) => {
        return (
            <TouchableOpacity
                key={`${index}`}
                style={styles.linkItem}
                onPress={() => this._handlePressLink(link)}
            >
                <LinearGradient
                    colors={['#62B794', '#4AA067']}
                    style={styles.linkItemInner}
                >
                    <Text style={styles.linkText}>{link.toUpperCase()}</Text>
                </LinearGradient>
            </TouchableOpacity>
        );
    }

    _handlePressLink = (link) => {
        this.props.navigation.navigate('SharedLink', { link });
    }
    render() {
        const { currentUser } = this.props.userState;
        const { firstName, lastName, city, distance, bio, links, dob } = currentUser;
        const { pictures } = currentUser;

        return (
            <ScrollView
                style={styles.container}
                contentContainerStyle={styles.contentContainer}
            >
                <Swiper
                    height={300}
                    showsPagination={true}
                    paginationStyle={{ position: 'absolute', top: 10, bottom: null, }}
                    dotStyle={{ backgroundColor: 'transparent', borderWidth: 1, borderColor: 'white', }}
                    activeDotColor={'white'}
                >
                    {
                        pictures.map((picture, index) => <Image
                            key={index}
                            style={styles.profileImage}
                            resizeMode={'cover'}
                            source={{ uri: picture }}
                        />)
                    }
                </Swiper>
                <Text style={styles.nameText}>
                    {`${firstName} ${lastName}`}
                    {
                        dob && <Text> ({moment().diff(moment(dob), 'years')})</Text>
                    }
                </Text>
                <Text style={styles.addressText}>{`${city} (${distance}mi)`}</Text>
                <Text style={styles.bioText}>{bio}</Text>
                <View style={styles.linksContainer}>
                    {
                        links.map((link, index) => this._renderLink(link, index))
                    }
                </View>
            </ScrollView>
        )
    }
}

MyProfileScreen.propTypes = {
    userState: PropTypes.shape({}),
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
    })
}

export default connectUser()(MyProfileScreen);