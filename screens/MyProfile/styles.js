import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        padding: 20,
    },

    profileImage: {
        flex: 1,
        borderRadius: 4,
    },
    nameText: {
        marginTop: 12,
        fontSize: 20,
    },
    addressText: {
        marginTop: 12,
        fontSize: 18,
    },
    bioText: {
        marginTop: 12,
        fontSize: 14,
        letterSpacing: 0.5,
    },
    linksContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 8,
        marginHorizontal: -6,
    },
    linkItem: {
        marginTop: 12,
        marginHorizontal: 6,
        flexGrow: 1,
        alignItems: 'center',
    },
    linkItemInner: {
        width: '100%',
        padding: 8,
        borderRadius: 4,
    },
    linkText: {
        color: '#FFF',
        fontSize: 14,
        textAlign: 'center',
    },
});

export default styles;