import { StyleSheet } from 'react-native';
import { BACKGROUND_GREEN } from '@AppConstants/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BACKGROUND_GREEN,
    },
    contentContainer: {
        flex: 1,
        padding: 20,
    },
    title: {
        marginTop: 8,
        fontSize: 24,
        textAlign: 'center',
    },
    link: {
        marginTop: 20,
        marginBottom: 8,
        alignItems: 'center',
        padding: 8,
        borderRadius: 4,
        alignSelf: 'center',
    },
    linkText: {
        color: '#FFF',
        fontSize: 14,
    },
    backButton: {
        position: 'absolute',
        left: 20,
        top: 20,
    },
    backButtonContent: {
        width: 40,
        height: 40,
        borderRadius: 20,
        backgroundColor: '#FFF',
        shadowOffset: { width: 0, height: 2, },
        shadowColor: '#888',
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default styles;