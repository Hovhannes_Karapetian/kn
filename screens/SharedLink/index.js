import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo';
import { SafeAreaView } from 'react-navigation';
import { Ionicons, } from '@expo/vector-icons';
import styles from './styles';
import { connectKeepers, connectUser, promisify } from '@AppRedux';
import { KeeperCard } from '@AppComponents';
import { includes, filter, concat } from 'lodash';
import { compose } from 'recompose';

class SharedLinkScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            sharedUsers: [],
        };
    }

    componentWillMount() {
        this._getSharedUsers();
    }

    _getSharedUsers = () => {
        const { link } = this.props.navigation.state.params;

        promisify(this.props.getSharedLink, { link })
            .then((res) => {
                this.setState({ sharedUsers: res });
            });
    }

    _keyExtractor = (item, index) => `${index}`;

    _renderItem = ({ item }) => {
        return (
            <KeeperCard data={item} onPress={() => { }} onToggleKeeper={this._handleToggleKeeper} />
        )
    }

    _handleToggleKeeper = (keeper) => {
        const { keepers, id } = this.props.userState.currentUser;
        const isMyKeeper = includes(keepers, keeper.id);

        let newKeepers = [];
        if (isMyKeeper) {
            newKeepers = filter(keepers, (item) => {
                return item != keeper.id;
            });
        } else {
            newKeepers = concat(keepers, keeper.id);
        }

        promisify(this.props.updateProfile, { keepers: newKeepers, userId: id });
    }

    _handlePressBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        const { sharedUsers } = this.state;
        const { link } = this.props.navigation.state.params;

        return (
            <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
                <View style={styles.contentContainer}>
                    <Text style={styles.title}>{'Shared Link'}</Text>
                    <LinearGradient
                        colors={['#62B794', '#4AA067']}
                        style={styles.link}
                    >
                        <Text style={styles.linkText}>{link.toUpperCase()}</Text>
                    </LinearGradient>
                    <FlatList
                        data={sharedUsers}
                        renderItem={this._renderItem}
                        keyExtractor={this._keyExtractor}
                    />
                    <TouchableOpacity
                        style={styles.backButton}
                        onPress={this._handlePressBack}
                    >
                        <View style={styles.backButtonContent}>
                            <Ionicons name="ios-arrow-back" size={24} color={'#888'} />
                        </View>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}

SharedLinkScreen.propTypes = {
    userState: PropTypes.shape({
        currentUser: PropTypes.shape({})
    }),
    keepersState: PropTypes.shape({
        allKeepers: PropTypes.arrayOf(PropTypes.shape({}))
    }),
    navigation: PropTypes.shape({
        navigate: PropTypes.func.isRequired,
        goBack: PropTypes.func.isRequired,
    })
}

export default compose(
    connectUser(),
    connectKeepers(),
)(SharedLinkScreen);