import React from 'react';
import { StyleSheet } from 'react-native';
import { BACKGROUND_GREEN } from '@AppConstants/colors';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: BACKGROUND_GREEN,
    }
});

export default styles;