import React from 'react';
import { View } from 'react-native';
import { SafeAreaView } from 'react-navigation';
import styles from './styles';
import { Header } from '@AppComponents';
import MyProfileScreen from '../MyProfile';
import KeepersScreen from '../Keepers';
import ChatListScreen from '../ChatList';
import AddLinkScreen from '../AddLink';

class HomeScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            activeTab: 2,
        }
    }

    _handleChangeTab = (tabIndex) => {
        if (tabIndex == 0) {
            this.props.navigation.openDrawer();
        } else {
            this.setState({ activeTab: tabIndex });
        }
    }

    render() {
        const { navigation } = this.props;
        const { activeTab } = this.state;
        return (
            <SafeAreaView style={styles.container} forceInset={{ top: 'always' }}>
                <Header
                    activeTab={activeTab}
                    onChangeTab={this._handleChangeTab}
                />
                {
                    activeTab == 1 && <MyProfileScreen navigation={navigation} />
                }
                {
                    activeTab == 2 && <KeepersScreen navigation={navigation} />
                }
                {
                    activeTab == 3 && <ChatListScreen navigation={navigation} />
                }
                {
                    activeTab == 4 && <AddLinkScreen navigation={navigation} />
                }
            </SafeAreaView>
        )
    }
}

export default HomeScreen;
