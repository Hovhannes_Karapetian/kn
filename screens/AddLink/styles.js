import { StyleSheet, Dimensions } from 'react-native';
import { BACKGROUND_GREEN, BUTTON_GREEN } from '@AppConstants/colors';
const { width, height } = Dimensions.get('window');


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        paddingBottom: 0,
    },
    searchContainer: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        alignItems: 'center',
        height: 48,
    },
    inner: {
        flex: 1,
        justifyContent: 'flex-end',
    },
    searchIcon: {

    },
    searchInput: {
        flex: 1,
        marginLeft: 12,
    },
    linksList: {
        marginTop: 8,
    },
    rowContainer: {
        flexDirection: 'row',
        height: 64,
        alignItems: 'center',
    },
    rowContentContainer: {
        flex: 1,
    },
    rowAddButton: {
        marginLeft: 8,
        marginRight: 4,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: '#FFF',
        shadowOffset: { width: 0, height: 2, },
        shadowColor: '#888',
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    rowLink: {
        fontSize: 20,
    },
    rowCategoryMarker: {
        marginTop: 8,
        fontSize: 16,
    }
});

export default styles;