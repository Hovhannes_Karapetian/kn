import React from 'react';
import PropTypes from 'prop-types';
import { View, FlatList, Text, TextInput, TouchableOpacity, KeyboardAvoidingView, Platform } from 'react-native';
import Toast from 'react-native-root-toast';
import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons';
import { compose } from 'recompose';
import styles from './styles';
import { connectUser, connectLinks, promisify } from '@AppRedux';
import { filter, lowerCase, includes, concat } from 'lodash';

const LinkRow = ({ dataSource, onPress }) => (
    <View style={styles.rowContainer}>
        <View style={styles.rowContentContainer}>
            <Text style={styles.rowLink} numberOfLines={1}>{dataSource.link}</Text>
            <Text style={styles.rowCategoryMarker} numberOfLines={1}>{dataSource.categoryMarker}</Text>
        </View>
        <TouchableOpacity onPress={onPress}>
            <View style={styles.rowAddButton}>
                <MaterialCommunityIcons name="plus-outline" size={24} color={'gray'} />
            </View>
        </TouchableOpacity>
    </View>
);

class AddLinkScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            searchStr: '',
            links: [],
        };
    }

    _keyExtractor = (item, index) => `${index}`;

    _renderItem = ({ item }) => {
        const { link } = item;
        return (
            <LinkRow dataSource={item} onPress={() => this._handleAddLink(link)} />
        )
    }

    _handleAddLink = (link) => {
        const { id, links } = this.props.userState.currentUser;

        if (!includes(links, link)) {
            promisify(this.props.updateProfile, { links: concat(links, link), userId: id })
                .then((response) => {
                    Toast.show('Link successfully added to your profile');
                })
                .catch((err) => {
                    console.log('Failed to add link to profile', err);
                });
        } else {
            Toast.show('Link already exists on your profile');
        }
    }

    _filterLinks = (query) => {
        if (query) {
            const { links } = this.props.linksState;
            const filteredLinks = filter(links, (o) => {
                return includes(lowerCase(o.link), lowerCase(query)) || includes(lowerCase(o.categoryMarker), lowerCase(query));
            });
            this.setState({ links: filteredLinks });
        } else {
            this.setState({ links: [] });
        }
    }

    _handleChangeQuery = (text) => {
        this.setState({ searchStr: text });
        this._filterLinks(text);
    }

    render() {
        const { searchStr, links } = this.state;

        return (
            <KeyboardAvoidingView style={{ flex: 1, }} behavior={Platform.OS === "ios" ? "padding" : null}>
                <View style={styles.inner}>
                    <View style={styles.container}>
                        <View style={styles.searchContainer}>
                            <Ionicons name="ios-search" size={32} />
                            <TextInput
                                style={styles.searchInput}
                                underlineColorAndroid={'transparent'}
                                value={searchStr}
                                onChangeText={this._handleChangeQuery}
                                autoCapitalize={'none'}
                                returnKeyType={'done'}
                            />
                        </View>
                        <FlatList
                            data={links}
                            renderItem={this._renderItem}
                            keyExtractor={this._keyExtractor}
                            style={styles.linksList}
                        />
                    </View>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default compose(
    connectUser(),
    connectLinks(),
)(AddLinkScreen);