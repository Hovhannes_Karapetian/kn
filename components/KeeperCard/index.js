import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { LinearGradient } from 'expo';
import styles from './styles';
import { connectUser } from '@AppRedux';
import { includes } from 'lodash';

class KeeperCard extends React.Component {
    _renderLink = (link, index) => {
        return (
            <View
                key={`${index}`}
                style={styles.linkItem}
            >
                <LinearGradient
                    colors={['#62B794', '#4AA067']}
                    style={styles.linkItemInner}
                >
                    <Text style={styles.linkText}>{link.toUpperCase()}</Text>
                </LinearGradient>
            </View>
        );
    }

    render() {
        const { onPress, onToggleKeeper, data } = this.props;
        const { firstName, lastName, age, city, distance, pictures, bio, links } = data;
        const { keepers } = this.props.userState.currentUser;
        const isMyKeeper = includes(keepers, data.id);
        return (
            <TouchableOpacity onPress={onPress}>
                <View style={styles.container}>
                    <View style={styles.profileContainer}>
                        <Image
                            style={styles.profileImage}
                            source={pictures && pictures.length > 0 ? { uri: pictures[0] } : require('@AppAssets/images/user.png')}
                            resizeMode={'cover'}
                        />
                        <View style={styles.profileInfoContainer}>
                            <Text style={styles.nameText}>{`${firstName} ${lastName}, ${age ? age : 26}`}</Text>
                            <Text style={styles.addressText}>{`${city ? city : 'Santa Monica, CA'} (${distance ? distance : 1.2}mi)`}</Text>
                            <Text style={styles.bioText}>{bio ? bio : 'Surfing, Fishing, Muscle Cars, and Fitness. If we can do all of this then we can be friends.'}</Text>
                        </View>
                    </View>
                    <View style={styles.linksContainer}>
                        {
                            links.map((link, index) => this._renderLink(link, index))
                        }
                    </View>
                    <TouchableOpacity style={[styles.symbolContainer, { backgroundColor: isMyKeeper ? '#27AC77' : '#FFF' }]} onPress={() => onToggleKeeper(this.props.data)}>
                        <Text style={[styles.symbolText, { color: isMyKeeper ? '#FFF' : '#27AC77' }]}>K</Text>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        );
    }
}

KeeperCard.propTypes = {
    data: PropTypes.shape({}),
    onToggleKeeper: PropTypes.func.isRequired,
    onPress: PropTypes.func.isRequired,
    userState: PropTypes.shape({
        currentUser: PropTypes.shape({}),
    }),
};

KeeperCard.defaultProps = {
    onToggleKeeper: () => { }
};

export default connectUser()(KeeperCard);