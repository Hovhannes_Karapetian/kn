import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        padding: 20,
        borderRadius: 4,
        backgroundColor: '#FFF',
    },
    profileContainer: {
        flexDirection: 'row',
    },
    profileInfoContainer: {
        marginLeft: 12,
        flex: 1,
    },
    profileImage: {
        width: 80,
        height: 80,
        borderRadius: 4,
    },
    nameText: {
        fontSize: 18,
    },
    addressText: {
        marginTop: 8,
        fontSize: 16,
    },
    bioText: {
        marginTop: 8,
        fontSize: 14,
    },
    linksContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 8,
        marginHorizontal: -6,
    },
    linkItem: {
        marginHorizontal: 6,
        marginTop: 12,
        flexGrow: 1,
        alignItems: 'center',
    },
    linkItemInner: {
        width: '100%',
        padding: 8,
        borderRadius: 4,
    },
    linkText: {
        color: '#FFF',
        fontSize: 14,
        textAlign: 'center',
    },
    symbolContainer: {
        position: 'absolute',
        right: 12,
        top: 12,
        backgroundColor: '#27AC77',
        width: 40,
        height: 40,
        borderRadius: 20,
        shadowOffset: { width: 0, height: 2, },
        shadowColor: '#888',
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
    },
    symbolText: {
        color: '#FFF',
        fontSize: 20,
        fontWeight: 'bold',
    }
});

export default styles;