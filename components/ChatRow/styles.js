import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        padding: 8,
        backgroundColor: 'transparent',
        height: 80,
        flexDirection: 'row',
        alignItems: 'center',
    },
    chatInfoContainer: {
        marginLeft: 12,
        flex: 1,
    },
    chatInfoHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    profileImage: {
        width: 64,
        height: 64,
        borderRadius: 32,
    },
    nameText: {
        fontSize: 18,
    },
    timeText: {
        fontSize: 16,
    },
    messageText: {
        marginTop: 8,
        fontSize: 14,
        color: '#888',
    },
});

export default styles;