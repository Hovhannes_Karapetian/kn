import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import styles from './styles';

const ChatRow = (props) => {
    const { onPress } = props;
    const { name, message, timestamp, pictures } = props.data;
    return (
        <TouchableOpacity onPress={() => onPress && onPress()}>
            <View style={styles.container}>
                <Image
                    style={styles.profileImage}
                    source={pictures && pictures.length > 0 ? { uri: pictures[0] } : require('@AppAssets/images/user.png')}
                    resizeMode={'cover'}
                />
                <View style={styles.chatInfoContainer}>
                    <View style={styles.chatInfoHeader}>
                        <Text style={styles.nameText}>{name}</Text>
                        <Text style={styles.timeText}>{timestamp}</Text>
                    </View>
                    <Text style={styles.messageText} numberOfLines={1} ellipsizeMode={'tail'}>{message}</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

export default ChatRow;