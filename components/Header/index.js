import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text, SafeAreaView } from 'react-native';
import { Ionicons, FontAwesome } from '@expo/vector-icons';
import styles from './styles';
import { ICON_GREEN } from '@AppConstants/colors';

class Header extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
        };
    }

    _handleChangeTab = (tabIndex) => {
        this.props.onChangeTab && this.props.onChangeTab(tabIndex);
    }

    render() {
        const { activeTab } = this.props;

        return (
            <View>
                <View style={styles.tabsContainer}>
                    <TouchableOpacity onPress={() => this._handleChangeTab(0)}>
                        <View style={styles.tabItem}>
                            <Ionicons name="ios-menu" size={24} color={activeTab == 0 ? ICON_GREEN : '#888'} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._handleChangeTab(1)}>
                        <View style={styles.tabItem}>
                            <FontAwesome name="user-o" size={24} color={activeTab == 1 ? ICON_GREEN : '#888'} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._handleChangeTab(2)}>
                        <View style={styles.tabItem}>
                            <FontAwesome name="users" size={24} color={activeTab == 2 ? ICON_GREEN : '#888'} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._handleChangeTab(3)}>
                        <View style={styles.tabItem}>
                            <FontAwesome name="comment-o" size={24} color={activeTab == 3 ? ICON_GREEN : '#888'} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this._handleChangeTab(4)}>
                        <View style={styles.tabItem}>
                            <FontAwesome name="plus" size={24} color={activeTab == 4 ? ICON_GREEN : '#888'} />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

Header.propTypes = {
    onChangeTab: PropTypes.func.isRequired,
    activeTab: PropTypes.number.isRequired,
}

Header.defaultProps = {
    onChangeTab: (tabIndex) => console.log('Tab selected at index:', tabIndex),
    activeTab: 2,
}
export default Header;