import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    tabsContainer: {
        height: 64,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 8,
    },
    tabItem: {
        width: 48,
        height: 48,
        borderRadius: 24,
        backgroundColor: '#FFF',
        shadowOffset: { width: 0, height: 2, },
        shadowColor: '#888',
        shadowOpacity: 1.0,
        shadowRadius: 2,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
    }
});

export default styles;