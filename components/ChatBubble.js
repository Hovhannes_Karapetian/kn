/* eslint no-use-before-define: ["error", { "variables": false }] */

import PropTypes from 'prop-types';
import React from 'react';
import { Text, Clipboard, StyleSheet, TouchableWithoutFeedback, View, ViewPropTypes } from 'react-native';

import { MessageText, MessageImage, MessageVideo, Time, Color, utils } from 'react-native-gifted-chat';
import { Svg } from 'expo';
import { moderateScale } from 'react-native-size-matters';

const { Path } = Svg;

export default class ChatBubble extends React.Component {

    onLongPress = () => {
        if (this.props.onLongPress) {
            this.props.onLongPress(this.context, this.props.currentMessage);
        } else if (this.props.currentMessage.text) {
            const options = ['Copy Text', 'Cancel'];
            const cancelButtonIndex = options.length - 1;
            this.context.actionSheet().showActionSheetWithOptions(
                {
                    options,
                    cancelButtonIndex,
                },
                (buttonIndex) => {
                    switch (buttonIndex) {
                        case 0:
                            Clipboard.setString(this.props.currentMessage.text);
                            break;
                        default:
                            break;
                    }
                },
            );
        }
    };

    handleBubbleToNext() {
        if (
            utils.isSameUser(this.props.currentMessage, this.props.nextMessage) &&
            utils.isSameDay(this.props.currentMessage, this.props.nextMessage)
        ) {
            return StyleSheet.flatten([
                styles[this.props.position].containerToNext,
                this.props.containerToNextStyle[this.props.position],
            ]);
        }
        return null;
    }

    handleBubbleToPrevious() {
        if (
            utils.isSameUser(this.props.currentMessage, this.props.previousMessage) &&
            utils.isSameDay(this.props.currentMessage, this.props.previousMessage)
        ) {
            return StyleSheet.flatten([
                styles[this.props.position].containerToPrevious,
                this.props.containerToPreviousStyle[this.props.position],
            ]);
        }
        return null;
    }

    renderMessageText() {
        if (this.props.currentMessage.text) {
            const { containerStyle, wrapperStyle, ...messageTextProps } = this.props;
            if (this.props.renderMessageText) {
                return this.props.renderMessageText(messageTextProps);
            }
            return <MessageText {...messageTextProps} />;
        }
        return null;
    }

    renderMessageImage() {
        if (this.props.currentMessage.image) {
            const { containerStyle, wrapperStyle, ...messageImageProps } = this.props;
            if (this.props.renderMessageImage) {
                return this.props.renderMessageImage(messageImageProps);
            }
            return <MessageImage {...messageImageProps} />;
        }
        return null;
    }

    renderMessageVideo() {
        if (this.props.currentMessage.video) {
            const { containerStyle, wrapperStyle, ...messageVideoProps } = this.props;
            if (this.props.renderMessageVideo) {
                return this.props.renderMessageVideo(messageVideoProps);
            }
            return <MessageVideo {...messageVideoProps} />;
        }
        return null;
    }

    renderTicks() {
        const { currentMessage } = this.props;
        if (this.props.renderTicks) {
            return this.props.renderTicks(currentMessage);
        }
        if (currentMessage.user._id !== this.props.user._id) {
            return null;
        }
        if (currentMessage.sent || currentMessage.received || currentMessage.pending) {
            return (
                <View style={styles.tickView}>
                    {currentMessage.sent && <Text style={[styles.tick, this.props.tickStyle]}>✓</Text>}
                    {currentMessage.received && <Text style={[styles.tick, this.props.tickStyle]}>✓</Text>}
                    {currentMessage.pending && <Text style={[styles.tick, this.props.tickStyle]}>🕓</Text>}
                </View>
            );
        }
        return null;
    }

    renderTime() {
        if (this.props.currentMessage.createdAt) {
            const { containerStyle, wrapperStyle, ...timeProps } = this.props;
            if (this.props.renderTime) {
                return this.props.renderTime(timeProps);
            }
            return <Time {...timeProps} />;
        }
        return null;
    }

    renderUsername() {
        const { currentMessage } = this.props;
        if (this.props.renderUsernameOnMessage) {
            if (currentMessage.user._id === this.props.user._id) {
                return null;
            }
            return (
                <View style={styles.usernameView}>
                    <Text style={[styles.username, this.props.usernameStyle]}>~ {currentMessage.user.name}</Text>
                </View>
            );
        }
        return null;
    }

    renderCustomView() {
        if (this.props.renderCustomView) {
            return this.props.renderCustomView(this.props);
        }
        return null;
    }

    renderArrow() {
        return (
            <View
                style={styles[this.props.position].arrowContainer}
            >
                <Svg style={styles[this.props.position].arrow}
                    width={moderateScale(15.5, 0.6)}
                    height={moderateScale(17.5, 0.6)}
                    viewBox="32.484 17.5 15.515 17.5"
                    enable-background="new 32.485 17.5 15.515 17.5"
                >
                    {
                        this.props.position == 'left' ?
                            <Path
                                d="M38.484,17.5c0,8.75,1,13.5-6,17.5C51.484,35,52.484,17.5,38.484,17.5z"
                                fill="#E5E5EA"
                                x="0"
                                y="0"
                            /> :
                            <Path
                                d="M48,35c-7-4-6-8.75-6-17.5C28,17.5,29,35,48,35z"
                                fill="#209C66"
                                x="0"
                                y="0"
                            />
                    }
                </Svg>
            </View>
        )
    }

    render() {
        return (
            <View style={[styles[this.props.position].container, this.props.containerStyle[this.props.position]]}>
                <View
                    style={[
                        styles[this.props.position].wrapper,
                        this.props.wrapperStyle[this.props.position],
                        // this.handleBubbleToNext(),
                        // this.handleBubbleToPrevious(),
                    ]}
                >
                    <TouchableWithoutFeedback
                        onLongPress={this.onLongPress}
                        accessibilityTraits="text"
                        {...this.props.touchableProps}
                    >
                        <View>
                            {this.renderArrow()}
                            {this.renderCustomView()}
                            {this.renderMessageImage()}
                            {this.renderMessageVideo()}
                            {this.renderMessageText()}
                            {/* <View style={[styles[this.props.position].bottom, this.props.bottomContainerStyle[this.props.position]]}>
                                {this.renderUsername()}
                                {this.renderTime()}
                                {this.renderTicks()}
                            </View> */}
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            </View>
        );
    }

}

const styles = {
    left: StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'flex-start',
        },
        wrapper: {
            borderRadius: 8,
            backgroundColor: '#E5E5EA',
            marginRight: 60,
            minHeight: 20,
            justifyContent: 'flex-end',
        },
        containerToNext: {
            borderBottomLeftRadius: 0,
        },
        containerToPrevious: {
            borderTopLeftRadius: 0,
        },
        bottom: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
        },
        arrowContainer: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: -1,
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-start'
        },
        arrow: {
            left: moderateScale(-6, 0.5),
        },
    }),
    right: StyleSheet.create({
        container: {
            flex: 1,
            alignItems: 'flex-end',
        },
        wrapper: {
            borderRadius: 8,
            backgroundColor: '#209C66',
            marginLeft: 60,
            minHeight: 20,
            justifyContent: 'flex-end',
        },
        containerToNext: {
            borderBottomRightRadius: 0,
        },
        containerToPrevious: {
            borderTopRightRadius: 0,
        },
        bottom: {
            flexDirection: 'row',
            justifyContent: 'flex-end',
        },
        arrowContainer: {
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            zIndex: -1,
            flex: 1,
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
        },
        arrow: {
            right: moderateScale(-6, 0.5),
        },
    }),
    tick: {
        fontSize: 10,
        backgroundColor: 'transparent',
        color: '#fff',
    },
    tickView: {
        flexDirection: 'row',
        marginRight: 10,
    },
    username: {
        top: -3,
        left: 0,
        fontSize: 12,
        backgroundColor: 'transparent',
        color: '#aaa',
    },
    usernameView: {
        flexDirection: 'row',
        marginHorizontal: 10,
    },
};

ChatBubble.contextTypes = {
    actionSheet: PropTypes.func,
};

ChatBubble.defaultProps = {
    touchableProps: {},
    onLongPress: null,
    renderMessageImage: null,
    renderMessageVideo: null,
    renderMessageText: null,
    renderCustomView: null,
    renderUsername: null,
    renderTicks: null,
    renderTime: null,
    position: 'left',
    currentMessage: {
        text: null,
        createdAt: null,
        image: null,
    },
    nextMessage: {},
    previousMessage: {},
    containerStyle: {},
    wrapperStyle: {},
    bottomContainerStyle: {},
    tickStyle: {},
    usernameStyle: {},
    containerToNextStyle: {},
    containerToPreviousStyle: {},
};

ChatBubble.propTypes = {
    user: PropTypes.object.isRequired,
    touchableProps: PropTypes.object,
    onLongPress: PropTypes.func,
    renderMessageImage: PropTypes.func,
    renderMessageVideo: PropTypes.func,
    renderMessageText: PropTypes.func,
    renderCustomView: PropTypes.func,
    renderUsernameOnMessage: PropTypes.bool,
    renderUsername: PropTypes.func,
    renderTime: PropTypes.func,
    renderTicks: PropTypes.func,
    position: PropTypes.oneOf(['left', 'right']),
    currentMessage: PropTypes.object,
    nextMessage: PropTypes.object,
    previousMessage: PropTypes.object,
    containerStyle: PropTypes.shape({
        left: ViewPropTypes.style,
        right: ViewPropTypes.style,
    }),
    wrapperStyle: PropTypes.shape({
        left: ViewPropTypes.style,
        right: ViewPropTypes.style,
    }),
    bottomContainerStyle: PropTypes.shape({
        left: ViewPropTypes.style,
        right: ViewPropTypes.style,
    }),
    tickStyle: Text.propTypes.style,
    usernameStyle: Text.propTypes.style,
    containerToNextStyle: PropTypes.shape({
        left: ViewPropTypes.style,
        right: ViewPropTypes.style,
    }),
    containerToPreviousStyle: PropTypes.shape({
        left: ViewPropTypes.style,
        right: ViewPropTypes.style,
    }),
};
