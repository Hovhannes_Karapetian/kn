
import { AsyncStorage } from 'react-native';

const BASE_URL = 'http://knnktapp.com/api/v1';

export const login = async (params) => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify(params)
    };
    try {
        const response = await fetch(`${BASE_URL}/auth/login`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const register = async (params) => {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: JSON.stringify(params)
    };
    try {
        const response = await fetch(`${BASE_URL}/auth/register`, options);
        const responseJson = await response.json();
        if (response.status === 201) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const getProfile = async (params) => {

    try {
        const accessToken = await AsyncStorage.getItem('@knnkt:access_token');
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            },
        };

        const response = await fetch(`${BASE_URL}/users/profile`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const updateProfile = async ({ userId, ...params }) => {

    try {
        const accessToken = await AsyncStorage.getItem('@knnkt:access_token');
        const options = {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            },
            body: JSON.stringify(params)
        };

        const response = await fetch(`${BASE_URL}/users/${userId}`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const uploadProfilePicture = async ({ userId, data }) => {

    try {
        const accessToken = await AsyncStorage.getItem('@knnkt:access_token');
        const options = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': `Bearer ${accessToken}`,
            },
            body: data,
        };

        const response = await fetch(`${BASE_URL}/users/upload`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const getSharedLink = async ({link}) => {

    try {
        const accessToken = await AsyncStorage.getItem('@knnkt:access_token');
        const options = {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            },
        };

        const response = await fetch(`${BASE_URL}/users/sharedLink?link=${link}`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}
