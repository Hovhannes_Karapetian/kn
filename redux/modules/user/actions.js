// @flow

import { createAction } from 'redux-actions';
import { createPromiseAction } from '../utils';

/**
 * Action Types
 */

export const USER_LOGIN_ATTEMPT = 'user/USER_LOGIN_ATTEMPT';
export const USER_LOGIN_SUCCESS = 'user/USER_LOGIN_SUCCESS';
export const USER_SIGNUP_ATTEMPT = 'user/USER_SIGNUP_ATTEMPT';
export const USER_SIGNUP_SUCCESS = 'user/USER_SIGNUP_SUCCESS';
export const GET_PROFILE_ATTEMPT = 'user/GET_PROFILE_ATTEMPT';
export const GET_PROFILE_SUCCESS = 'user/GET_PROFILE_SUCCESS';
export const UPDATE_PROFILE_ATTEMPT = 'user/UPDATE_PROFILE_ATTEMPT';
export const UPDATE_PROFILE_SUCCESS = 'user/UPDATE_PROFILE_SUCCESS';
export const UPLOAD_PROFILE_PICTURE_ATTEMPT = 'user/UPLOAD_PROFILE_PICTURE_ATTEMPT';
export const UPLOAD_PROFILE_PICTURE_SUCCESS = 'user/UPLOAD_PROFILE_PICTURE_SUCCESS';
export const CONFIRM_SMS_ATTEMPT = 'user/CONFIRM_SMS_ATTEMPT';
export const CONFIRM_SMS_SUCCESS = 'user/CONFIRM_SMS_SUCCESS';
export const FORGOT_PASSWORD_ATTEMPT = 'user/FORGOT_PASSWORD_ATTEMPT';
export const FORGOT_PASSWORD_SUCCESS = 'user/FORGOT_PASSWORD_SUCCESS';
export const GET_SHARED_LINK_ATTEMPT = 'user/GET_SHARED_LINK_ATTEMPT';
export const GET_SHARED_LINK_SUCCESS = 'user/GET_SHARED_LINK_SUCCESS';

/**
 * Action Creators
 */
export const userActionCreators = {
  userLogin: createPromiseAction(USER_LOGIN_ATTEMPT),
  userLoginSuccess: createAction(USER_LOGIN_SUCCESS),
  userSignup: createPromiseAction(USER_SIGNUP_ATTEMPT),
  userSignupSuccess: createAction(USER_SIGNUP_SUCCESS),
  confirmSMS: createPromiseAction(CONFIRM_SMS_ATTEMPT),
  confirmSMSSuccess: createAction(CONFIRM_SMS_SUCCESS),
  forgotPassword: createPromiseAction(FORGOT_PASSWORD_ATTEMPT),
  forgotPasswordSuccess: createAction(FORGOT_PASSWORD_SUCCESS),
  updateProfile: createPromiseAction(UPDATE_PROFILE_ATTEMPT),
  updateProfileSuccess: createAction(UPDATE_PROFILE_SUCCESS),
  uploadProfilePicture: createPromiseAction(UPLOAD_PROFILE_PICTURE_ATTEMPT),
  uploadProfilePictureSuccess: createAction(UPLOAD_PROFILE_PICTURE_SUCCESS),
  getProfile: createPromiseAction(GET_PROFILE_ATTEMPT),
  getProfileSuccess: createAction(GET_PROFILE_SUCCESS),
  getSharedLink: createPromiseAction(GET_SHARED_LINK_ATTEMPT),
  getSharedLinkSuccess: createAction(GET_SHARED_LINK_SUCCESS),
};
