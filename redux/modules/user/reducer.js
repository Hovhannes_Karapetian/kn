// @flow

import { AsyncStorage } from 'react-native';
import {
  USER_LOGIN_SUCCESS, USER_SIGNUP_SUCCESS, CONFIRM_SMS_SUCCESS, GET_PROFILE_SUCCESS, UPDATE_PROFILE_SUCCESS, UPLOAD_PROFILE_PICTURE_SUCCESS,
} from './actions';
import { defaultReducers } from '../defaultReducers';

const DEFAULT = defaultReducers.userState;

export default function userState(state = DEFAULT, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case USER_LOGIN_SUCCESS:
      AsyncStorage.setItem('@knnkt:access_token', payload.token.accessToken);
      AsyncStorage.setItem('@knnkt:refresh_token', payload.token.refreshToken);
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          ...payload.user,
        },
      };
    case USER_SIGNUP_SUCCESS:
      AsyncStorage.setItem('@knnkt:access_token', payload.token.accessToken);
      AsyncStorage.setItem('@knnkt:refresh_token', payload.token.refreshToken);
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          ...payload.user,
        },
      };
    case GET_PROFILE_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          ...payload,
        }
      };
    case UPDATE_PROFILE_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          ...payload,
        }
      };
    case UPLOAD_PROFILE_PICTURE_SUCCESS:
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          ...payload,
        }
      };
    case CONFIRM_SMS_SUCCESS:
      AsyncStorage.setItem('@knnkt:access_token', 'payload.authToken');
      return state;
    default:
      return state;
  }
}
