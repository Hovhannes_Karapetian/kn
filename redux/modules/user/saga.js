// @flow

import {
  take,
  put,
  call,
  fork,
  all,
} from 'redux-saga/effects';

import {
  userActionCreators,
  USER_LOGIN_ATTEMPT,
  USER_SIGNUP_ATTEMPT,
  CONFIRM_SMS_ATTEMPT,
  UPDATE_PROFILE_ATTEMPT,
  GET_PROFILE_ATTEMPT,
  UPLOAD_PROFILE_PICTURE_ATTEMPT,
  GET_SHARED_LINK_ATTEMPT,
} from './actions';

import { login, register, updateProfile, getProfile, uploadProfilePicture, getSharedLink } from './connections';

export function* asyncUserLoginAttempt({ payload, resolve, reject }) {
  try {
    const response = yield call(login, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(userActionCreators.userLoginSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchUserLoginAttempt() {
  while (true) {
    const action = yield take(USER_LOGIN_ATTEMPT);
    yield* asyncUserLoginAttempt(action);
  }
}

export function* asyncUserSignupAttempt({ payload, resolve, reject }) {
  try {
    const response = yield call(register, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(userActionCreators.userSignupSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchUserSignupAttempt() {
  while (true) {
    const action = yield take(USER_SIGNUP_ATTEMPT);
    yield* asyncUserSignupAttempt(action);
  }
}

export function* asyncConfirmSMSAttempt({ payload, resolve, reject }) {
  yield put(userActionCreators.confirmSMSSuccess());
  resolve();
}

export function* watchConfirmSMSAttempt() {
  while (true) {
    const action = yield take(CONFIRM_SMS_ATTEMPT);
    yield* asyncConfirmSMSAttempt(action);
  }
}

export function* asyncGetUserProfile({ payload, resolve, reject }) {

  try {
    const response = yield call(getProfile, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(userActionCreators.getProfileSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchGetUserProfileAttempt() {
  while (true) {
    const action = yield take(GET_PROFILE_ATTEMPT);
    yield* asyncGetUserProfile(action);
  }
}

export function* asyncUpdateUserProfile({ payload, resolve, reject }) {

  try {
    const response = yield call(updateProfile, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(userActionCreators.updateProfileSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchUpdateUserProfileAttempt() {
  while (true) {
    const action = yield take(UPDATE_PROFILE_ATTEMPT);
    yield* asyncUpdateUserProfile(action);
  }
}

export function* asyncUploadProfilePicture({ payload, resolve, reject }) {

  try {
    const response = yield call(uploadProfilePicture, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(userActionCreators.uploadProfilePictureSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchUploadProfilePictureAttempt() {
  while (true) {
    const action = yield take(UPLOAD_PROFILE_PICTURE_ATTEMPT);
    yield* asyncUploadProfilePicture(action);
  }
}

export function* asyncGetSharedLink({ payload, resolve, reject }) {

  try {
    const response = yield call(getSharedLink, payload);
    if (response.error) {
      reject(response.err);
    } else {
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchGetSharedLinksAttempt() {
  while (true) {
    const action = yield take(GET_SHARED_LINK_ATTEMPT);
    yield* asyncGetSharedLink(action);
  }
}

export default function* () {
  yield all([
    fork(watchUserLoginAttempt),
    fork(watchUserSignupAttempt),
    fork(watchConfirmSMSAttempt),
    fork(watchGetUserProfileAttempt),
    fork(watchUpdateUserProfileAttempt),
    fork(watchUploadProfilePictureAttempt),
    fork(watchGetSharedLinksAttempt),
  ]);
}
