// @flow

import { AsyncStorage } from 'react-native';
import {
  FETCH_LINKS_SUCCESS,
} from './actions';
import { defaultReducers } from '../defaultReducers';

const DEFAULT = defaultReducers.linksState;

export default function linksState(state = DEFAULT, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case FETCH_LINKS_SUCCESS:
      return {
        ...state,
        links: payload,
      };
    default:
      return state;
  }
}
