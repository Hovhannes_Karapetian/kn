// @flow

import {
  take,
  put,
  call,
  fork,
  all,
} from 'redux-saga/effects';

import {
  linksActionCreators,
  FETCH_LINKS_ATTEMPT,
  SUGGEST_LINK_ATTEMPT,
} from './actions';

import { fetchLinks, suggestLink } from './connections';

export function* asyncFetchLinks({ payload, resolve, reject }) {
  try {
    const response = yield call(fetchLinks, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(linksActionCreators.fetchLinksSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchFetchLinksAttempt() {
  while (true) {
    const action = yield take(FETCH_LINKS_ATTEMPT);
    yield* asyncFetchLinks(action);
  }
}
export function* asyncSuggestLink({ payload, resolve, reject }) {
  try {
    const response = yield call(suggestLink, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(linksActionCreators.suggestLinkSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchSuggestLinkAttempt() {
  while (true) {
    const action = yield take(SUGGEST_LINK_ATTEMPT);
    yield* asyncSuggestLink(action);
  }
}


export default function* () {
  yield all([
    fork(watchFetchLinksAttempt),
    fork(watchSuggestLinkAttempt),
  ]);
}
