import { AsyncStorage } from 'react-native';

const BASE_URL = 'http://knnktapp.com/api/v1';

export const fetchLinks = async (params) => {
    const options = {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
    };
    try {
        const response = await fetch(`${BASE_URL}/links`, options);
        const responseJson = await response.json();
        if (response.status === 200) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}

export const suggestLink = async (params) => {

    try {
        const accessToken = await AsyncStorage.getItem('@knnkt:access_token');
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Bearer ${accessToken}`,
            },
            body: JSON.stringify(params)
        };
        const response = await fetch(`${BASE_URL}/links/suggest`, options);
        const responseJson = await response.json();
        if (response.status === 201) {
            return responseJson;
        }
        return {
            error: true,
            status: response.status,
            err: responseJson
        };
    } catch (err) {
        return {
            error: true,
            msg: 'Network error',
            err
        };
    }
}