import { connect } from 'react-redux';
import { linksActionCreators } from './actions';

function mapStateToProps({ linksState }) {
  return {
    linksState
  };
}

const mapDispatchToProps = linksActionCreators;

export function connectLinks(configMapStateToProps = mapStateToProps) {
  return connect(
    configMapStateToProps,
    mapDispatchToProps
  );
}
