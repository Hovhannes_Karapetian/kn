// @flow

import { createAction } from 'redux-actions';
import { createPromiseAction } from '../utils';

/**
 * Action Types
 */

export const FETCH_LINKS_ATTEMPT = 'links/FETCH_LINKS_ATTEMPT';
export const FETCH_LINKS_SUCCESS = 'links/FETCH_LINKS_SUCCESS';
export const SUGGEST_LINK_ATTEMPT = 'links/SUGGEST_LINK_ATTEMPT';
export const SUGGEST_LINK_SUCCESS = 'links/SUGGEST_LINK_SUCCESS';

/**
 * Action Creators
 */
export const linksActionCreators = {
  fetchLinks: createPromiseAction(FETCH_LINKS_ATTEMPT),
  fetchLinksSuccess: createAction(FETCH_LINKS_SUCCESS),
  suggestLink: createPromiseAction(SUGGEST_LINK_ATTEMPT),
  suggestLinkSuccess: createAction(SUGGEST_LINK_SUCCESS),
};
