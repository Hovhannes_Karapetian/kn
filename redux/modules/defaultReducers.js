// @flow

const userState = {
  currentUser: {
    firstName: "",
    lastName: "",
    pictures: [],
    age: 23,
    city: "Los Angeles, CA",
    distance: 0, // format as "(0mi)"
    bio: "",
    links: []
  },
};

const keepersState = {
  allKeepers: []
}

const chatsState = {
  allChats: [
    {
      name: "Sarah Farley",
      pictures: ['https://trello-attachments.s3.amazonaws.com/5bf3001739c5de453465e511/5c672babb943c44006bb76ec/78c123ebd6024a0009dffd92ff4b305d/user2.png'],
      timestamp: '2:53 PM',
      message: 'Hey there, how are you?',
    },
    {
      name: "Sarah Farley",
      pictures: ['https://trello-attachments.s3.amazonaws.com/5bf3001739c5de453465e511/5c672babb943c44006bb76ec/78c123ebd6024a0009dffd92ff4b305d/user2.png'],
      timestamp: '7:52 AM',
      message: 'This shows how longer messages get cutt tttt tttt tttt ttt tttt ttt',
    },
    {
      name: "Sarah Farley",
      pictures: ['https://trello-attachments.s3.amazonaws.com/5bf3001739c5de453465e511/5c672babb943c44006bb76ec/78c123ebd6024a0009dffd92ff4b305d/user2.png'],
      timestamp: 'Wed',
      message: 'Hey there, how are you?',
    },
    {
      name: "Sarah Farley",
      pictures: ['https://trello-attachments.s3.amazonaws.com/5bf3001739c5de453465e511/5c672babb943c44006bb76ec/78c123ebd6024a0009dffd92ff4b305d/user2.png'],
      timestamp: 'Mon',
      message: 'Hey there, how are you?',
    },
  ]
}

const linksState = {
  links: [],
}


export const defaultReducers = {
  userState,
  keepersState,
  chatsState,
  linksState,
};
