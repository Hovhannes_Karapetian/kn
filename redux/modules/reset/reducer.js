// @flow

import { AsyncStorage } from 'react-native';
import { LOGOUT } from '../sharedActions';

import { defaultReducers } from '../defaultReducers';

export default function resetReducer(state, action) {
  switch (action.type) {
    case LOGOUT: {
      // Remove access token from AsyncStorage
      AsyncStorage.removeItem('@knnkt:access_token');
      AsyncStorage.removeItem('@knnkt:refresh_token');
      // Reset redux-store
      return {
        ...state,
        ...defaultReducers
      };
    }
    default:
      return state;
  }
}
