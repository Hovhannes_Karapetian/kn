// @flow

import { AsyncStorage } from 'react-native';
import {
  GET_KEEPERS_SUCCESS,
} from './actions';
import { defaultReducers } from '../defaultReducers';

const DEFAULT = defaultReducers.keepersState;

export default function keepersState(state = DEFAULT, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case GET_KEEPERS_SUCCESS:
      return {
        ...state,
        allKeepers: payload,
      };
    default:
      return state;
  }
}
