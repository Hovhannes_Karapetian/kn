import { connect } from 'react-redux';
import { keepersActionCreators } from './actions';

function mapStateToProps({ keepersState }) {
  return {
    keepersState
  };
}

const mapDispatchToProps = keepersActionCreators;

export function connectKeepers(configMapStateToProps = mapStateToProps) {
  return connect(
    configMapStateToProps,
    mapDispatchToProps
  );
}
