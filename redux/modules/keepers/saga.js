// @flow

import {
  take,
  put,
  call,
  fork,
  all,
} from 'redux-saga/effects';

import {
  keepersActionCreators,
  GET_KEEPERS_ATTEMPT,
} from './actions';

import { getKeepers } from './connections';

export function* asyncGetKeepersAttempt({ payload, resolve, reject }) {
  try {
    const response = yield call(getKeepers, payload);
    if (response.error) {
      reject(response.err);
    } else {
      yield put(keepersActionCreators.getKeepersSuccess(response));
      resolve(response);
    }
  } catch (error) {
    reject(error);
  }
}

export function* watchGetKeepersAttempt() {
  while (true) {
    const action = yield take(GET_KEEPERS_ATTEMPT);
    yield* asyncGetKeepersAttempt(action);
  }
}

export default function* () {
  yield all([
    fork(watchGetKeepersAttempt),
  ]);
}
