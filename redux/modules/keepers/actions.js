// @flow

import { createAction } from 'redux-actions';
import { createPromiseAction } from '../utils';

/**
 * Action Types
 */

export const GET_KEEPERS_ATTEMPT = 'keepers/GET_KEEPERS_ATTEMPT';
export const GET_KEEPERS_SUCCESS = 'keepers/GET_KEEPERS_SUCCESS';

/**
 * Action Creators
 */
export const keepersActionCreators = {
  getKeepers: createPromiseAction(GET_KEEPERS_ATTEMPT),
  getKeepersSuccess: createAction(GET_KEEPERS_SUCCESS),
};
