// @flow

import {
  take,
  put,
  call,
  fork,
  all,
} from 'redux-saga/effects';

import {
  chatsActionCreators,
  GET_CHATS_ATTEMPT,
} from './actions';

export function* asyncGetChatsAttempt({ payload, resolve, reject }) {
  resolve()
}

export function* watchGetChatsAttempt() {
  while (true) {
    const action = yield take(GET_CHATS_ATTEMPT);
    yield* asyncGetChatsAttempt(action);
  }
}

export default function* () {
  yield all([
    fork(watchGetChatsAttempt),
  ]);
}
