import { connect } from 'react-redux';
import { chatsActionCreators } from './actions';

function mapStateToProps({ chatsState }) {
  return {
    chatsState
  };
}

const mapDispatchToProps = chatsActionCreators;

export function connectChats(configMapStateToProps = mapStateToProps) {
  return connect(
    configMapStateToProps,
    mapDispatchToProps
  );
}
