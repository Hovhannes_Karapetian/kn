// @flow

import {
  GET_CHATS_SUCCESS,
} from './actions';
import { defaultReducers } from '../defaultReducers';

const DEFAULT = defaultReducers.chatsState;

export default function chatsState(state = DEFAULT, action = {}) {
  const { type, payload } = action;

  switch (type) {
    case GET_CHATS_SUCCESS:
      return state
    default:
      return state;
  }
}
