// @flow

import { createAction } from 'redux-actions';
import { createPromiseAction } from '../utils';

/**
 * Action Types
 */

export const GET_CHATS_ATTEMPT = 'chats/GET_CHATS_ATTEMPT';
export const GET_CHATS_SUCCESS = 'chats/GET_CHATS_SUCCESS';

/**
 * Action Creators
 */
export const chatsActionCreators = {
  getChats: createPromiseAction(GET_CHATS_ATTEMPT),
  getChatsSuccess: createAction(GET_CHATS_SUCCESS),
};
