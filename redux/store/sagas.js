// @flow

import { fork, all } from 'redux-saga/effects';
import { userSaga, keepersSaga, chatsSaga, linksSaga, } from '../modules';

export default function* rootSaga() {
  yield all([
    fork(userSaga),
    fork(keepersSaga),
    fork(chatsSaga),
    fork(linksSaga),
  ]);
}
