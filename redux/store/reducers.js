// @flow

import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { LOGOUT } from '../modules/sharedActions';

import {
  userState,
  keepersState,
  resetReducer,
  chatsState,
  linksState,
} from '../modules';

const config = {
  version: 0,
  key: '@Knnkt_redux_store',
  storage
};

const appReducer = persistCombineReducers(config, {
  userState,
  keepersState,
  chatsState,
  linksState,
});

export default function rootReducer(state, action) {
  let finalState = appReducer(state, action);

  if (action.type === LOGOUT) {
    finalState = resetReducer(finalState, action);
  }

  return finalState;
}
