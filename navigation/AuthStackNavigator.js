import React from 'react';
import { createStackNavigator, } from 'react-navigation';

import { WelcomeScreen, LoginScreen, SignupScreen, ConfirmSMSScreen } from '../screens/Auth';

export default createStackNavigator(
    {
        Welcome: WelcomeScreen,
        Login: LoginScreen,
        Signup: SignupScreen,
        ConfirmSMS: ConfirmSMSScreen,
    },
    {
        headerMode: 'none',
    }
);