import React from 'react';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';

import HomeScreen from '../screens/Home/HomeScreen';
import SideMenu from '../screens/SideMenu/SideMenu';
import MessagesScreen from '../screens/Messages';
import SharedLinkScreen from '../screens/SharedLink';
import EditProfileScreen from '../screens/EditProfile';
import KeeperProfileScreen from '../screens/KeeperProfile';

const HomeDrawerNavigator = createDrawerNavigator(
  {
    Home: HomeScreen,
  }, {
    contentComponent: SideMenu
  });

export default createStackNavigator(
  {
    HomeDrawer: HomeDrawerNavigator,
    Messages: MessagesScreen,
    SharedLink: SharedLinkScreen,
    EditProfile: EditProfileScreen,
    KeeperProfile: KeeperProfileScreen,
  },
  {
    headerMode: 'none',
  }
);